require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |t|
  t.rspec_opts = [].tap do |a|
    a << '--color'
    a << '--format documentation'
    a << '--backtrace'
    a << "--default-path test"
    a << '-I test/spec'
  end.join(' ')
end
